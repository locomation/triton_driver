//------------------------------------------------------------------------------
/// @file TritonNode.h
/// @author     Arunabh Sharma <arunabh@locomation.ai>
/// @brief      ROS wrapper around the Triton driver
/// @version    0.1
/// @date       2019-03-21
///
/// @copyright  Copyright (c) 2019 Locomation, Inc.\n All Rights Reserved.\n
///             CONFIDENTIAL and PROPRIETARY\n
/// @par
///
/// @internal
/// @par History
/// * 2019-03-21 Arunabh Sharma <arunabh@locomation.ai> Created file.
///   //////////////////////////////////////////////////////////////////////////

#ifndef TRITONDRIVER_TRITON_TRITONNODE_H_
#define TRITONDRIVER_TRITON_TRITONNODE_H_

#include <memory>
#include <string>
#include <vector>

#include <sensor_msgs/Image.h>
#include "ros/ros.h"

#include "triton/TritonDriver.h"

namespace triton
{
  /**
   * @brief      Ros Node Wrapper around the triton driver class
   *
   * @ingroup    camera
   * @author     Arunabh Sharma
   * @attention  This is for internal use only, DO NOT distribute the code
   */
  class TritonNode
  {
   public:
    /**
     * @brief      Deleting the default constructor.
     */
    TritonNode() = delete;
    /**
     * @brief      Constructs the ros node object for the triton driver
     *
     * @param[in]  node_name   Name of topic onto which node will publish
     * @param[in]  nh          Object that represents the ROS Node
     * @param[in]  n_frames    Number of frames to get from camera, -1 means
     *                         free run
     * @param[in]  spin_freq_  Spin frequency of node(not used right now)
     */
    TritonNode(const std::string node_name,
               ros::NodeHandle nh,
               const int n_frames   = -1,
               const int spin_freq_ = 30);

    /**
     * @brief      Destroys the object.
     */
    ~TritonNode();
    /**
     * @brief      Reads an image message from camera and converts it to ROS
     *             Message
     *
     * @param[in]  img_msg_ptr  The image message pointer
     */
    void ReadImgMessage(sensor_msgs::ImagePtr img_msg_ptr);
    /**
     * @brief      Spins the ros node
     */
    bool Start();

   private:
    std::string node_name_;  ///< Name of the topic which node will publish onto
    int n_frames_;  ///< Number of frames to get from camera, -1 means free run
    std::unique_ptr<TritonDriver>
        td_inst_;               ///< Instance of the triton driver object
    ros::Publisher image_pub_;  ///< Publisher object from ROS
    ros::Rate spin_freq_;       ///< Spin rate object from ROS
    ros::NodeHandle nh_;        ///< Object that represents Node
    bool run_node_;
    std::string mac_address_;
  };  // class triton node
}  // namespace triton

#endif  // TRITONDRIVER_TRITON_TRITONNODE_H_