//------------------------------------------------------------------------------
/// @file TritonNodelet.h
/// @author     Arunabh Sharma <arunabh@locomation.ai>
/// @brief      Nodelet class for the lucid vision driver
/// @version    0.1
/// @date       2019-03-21
///
/// @copyright  Copyright (c) 2019 Locomation, Inc.\n All Rights Reserved.\n
///             CONFIDENTIAL and PROPRIETARY\n
/// @par
///
/// @internal
/// @par History
/// * 2019-03-21 Arunabh Sharma <arunabh@locomation.ai> Created file.
///   //////////////////////////////////////////////////////////////////////////

#ifndef TRITONDRIVER_TRITON_TRITONNODELET_H_
#define TRITONDRIVER_TRITON_TRITONNODELET_H_

#include "nodelet/nodelet.h"

#include <memory>
#include <thread>

// #include "boost/thread.hpp"

#include "triton/TritonNode.h"

namespace triton
{
  /**
   * @brief      Ros Nodelet Wrapper around the triton node class
   *
   * @ingroup    camera
   * @author     Arunabh Sharma
   * @attention  This is for internal use only, DO NOT distribute the code
   */
  class TritonNodelet : public nodelet::Nodelet
  {
   public:
    /**
     * @brief      Constructs the object.
     */
    TritonNodelet();

    /**
     * @brief      Destroys the object.
     */
    ~TritonNodelet();

    /**
     * @brief      Object that initializes the nodelet
     */
    virtual void onInit();

   private:
    std::shared_ptr<TritonNode>
        tn_inst_;  ///< Instance of the triton node object
    std::thread
        img_thread_;  /// Thread to bind start method of ros node to it's object

  };  // class triton nodelet
}  // namespace triton

#endif  // TRITONDRIVER_TRITON_TRITONNODELET_H_