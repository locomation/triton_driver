////////////////////////////////////////////////////////////////////////////////
/// @file TritonNodelet.cpp
/// @author Arunabh Sharma <arunabh@locomation.ai>
/// @brief Nodelet class for the lucid vision driver cpp file
/// @version 0.1
/// @date 2019-03-22
///
/// @copyright
/// Copyright (c) 2019 Locomation, Inc.\n
/// All Rights Reserved.\n
/// CONFIDENTIAL and PROPRIETARY\n
/// @par
///
/// @internal
/// @par History
/// * 2019-03-22 Arunabh Sharma <arunabh@locomation.ai> Created file.
////////////////////////////////////////////////////////////////////////////////

#include "triton/TritonNodelet.h"

#include "pluginlib/class_list_macros.h"

#include "triton/TritonNode.h"

namespace triton
{
TritonNodelet::TritonNodelet()
{
}

TritonNodelet::~TritonNodelet()
{
}

void TritonNodelet::onInit()
{
    NODELET_DEBUG("Initializing nodelet");
    ros::NodeHandle nh = getNodeHandle();
    tn_inst_.reset(new TritonNode("TritonPub", nh, -1, -1));
    img_thread_ = std::thread(std::bind(&TritonNode::Start, tn_inst_.get()));
}
}  // namespace triton

PLUGINLIB_EXPORT_CLASS(triton::TritonNodelet, nodelet::Nodelet)