////////////////////////////////////////////////////////////////////////////////
/// @file TritonNode.cpp
/// @author Arunabh Sharma <arunabh@locomation.ai>
/// @brief Class for the lucid vision driver ros node
/// @version 0.1
/// @date 2019-03-21
///
/// @copyright
/// Copyright (c) 2019 Locomation, Inc.\n
/// All Rights Reserved.\n
/// CONFIDENTIAL and PROPRIETARY\n
/// @par
///
/// @internalv
/// @par History
/// * 2019-03-21 Arunabh Sharma <arunabh@locomation.ai> Created file.
////////////////////////////////////////////////////////////////////////////////

#include "triton/TritonNode.h"

#include <functional>
#include <memory>
#include <string>

#include "ros/ros.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/image_encodings.h"

#include "triton/TritonDefinitions.h"
#include "triton/TritonDriver.h"

namespace triton
{
  TritonNode::TritonNode(const std::string node_name,
                         ros::NodeHandle nh,
                         const int n_frames,
                         const int spin_freq)
      : node_name_(node_name)
      , n_frames_(n_frames)
      , spin_freq_(spin_freq)
      , nh_(nh)
      , run_node_(false)
  {
    image_pub_ = nh_.advertise<sensor_msgs::Image>(node_name_, 10);
    if (nh_.getParam("/triton/mac_address", mac_address_))
    {
      ROS_INFO("given mac address %lu", mac_address_);
    }
    else
    {
      ROS_ERROR("Failed to get parametere mac_address");
    }
    td_inst_ = std::make_unique<TritonDriver>();
  }

  TritonNode::~TritonNode()
  {
    td_inst_->DestroyDevice();
  }

  void TritonNode::ReadImgMessage(sensor_msgs::ImagePtr img_msg_ptr)
  {
    const int image_timeout = 2000;
    td_inst_->GetImage(image_timeout, img_msg_ptr->data);
    img_msg_ptr->header.stamp = ros::Time().fromNSec(td_inst_->GetTimestamp());
    ROS_INFO("%lu", td_inst_->GetTimestamp());

    img_msg_ptr->height   = td_inst_->GetHeight();
    img_msg_ptr->width    = td_inst_->GetWidth();
    img_msg_ptr->encoding = sensor_msgs::image_encodings::BAYER_RGGB8;
    img_msg_ptr->step     = td_inst_->GetWidth() * td_inst_->GetBytesPerPixel();

    ROS_INFO("First pixel of image message = %s",
             (std::to_string(img_msg_ptr->data[0])).c_str());
  }

  bool TritonNode::Start()
  {  // The order in which the image is acquired is connect device -> configure
    // stream -> get image -> reallocate buffer -> destory device when you're
    // done
    const int num_img_msg     = 10;
    const int device_timeout  = 100;
    const int iw              = 2048;
    const int ih              = 1536;
    const bool ptp_enable     = false;
    const double exposure_val = 25000.0;
    const double gain_val     = 20.0;

    std::vector<sensor_msgs::ImagePtr> img_msg_ptr_vec(
        num_img_msg, boost::make_shared<sensor_msgs::Image>());

    if (n_frames_ == -1 || n_frames_ > 0)
    {
      run_node_ = true;
      if (!td_inst_->ConnectToDevice(std::stoull(mac_address_, 0, 16),
                                     device_timeout))
      {
        ROS_WARN("Unable to get camera device from ros");
        return false;
      }
      if (!td_inst_->ConfigureAndStartStream(
              iw,
              ih,
              AcquisitionModeVal[ACQUISITION_CONTINUOUS],
              StreamBufferHandlingModeVal[NEWEST_ONLY],
              ptp_enable))
      {
        ROS_WARN("Unable to configure camera stream from ros");
        return false;
      }
      td_inst_->SetExposureTime(exposure_val);
      td_inst_->SetGain(gain_val);
      const int frame_size = iw * ih;
      for (int i = 0; i < num_img_msg; i++)
      {
        img_msg_ptr_vec[i]->data.resize(frame_size);
      }
    }
    else
    {
      ROS_INFO("Invalid frame number specification");
      return false;
    }
    while (nh_.ok() && run_node_)
    {
      int img_msg_cycle = 0;
      ReadImgMessage(img_msg_ptr_vec[img_msg_cycle]);
      // sensor_msgs::Image img_msg;
      image_pub_.publish(img_msg_ptr_vec[img_msg_cycle]);
      if (n_frames_ != -1)
      {
        if (n_frames_)
        {
          n_frames_--;
        }
        else
        {
          run_node_ = false;
        }
      }
      if (img_msg_cycle++ == 10)
      {
        img_msg_cycle = 0;
      }
    }
    return true;
  }
}  // namespace triton