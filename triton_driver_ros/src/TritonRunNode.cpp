////////////////////////////////////////////////////////////////////////////////
/// @file TritonRunNode.cpp
/// @author Arunabh Sharma <arunabh@locomation.ai>
/// @brief Code to run the triton node.
/// @version 0.1
/// @date 2019-03-29
///
/// @copyright
/// Copyright (c) 2019 Locomation, Inc.\n
/// All Rights Reserved.\n
/// CONFIDENTIAL and PROPRIETARY\n
/// @par
///
/// @internal
/// @par History
/// * 2019-03-29 Arunabh Sharma <arunabh@locomation.ai> Created file.
////////////////////////////////////////////////////////////////////////////////

#include "ros/ros.h"

#include "triton/TritonNode.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "triton_node_publisher");

    ros::NodeHandle nh_inst;
    triton::TritonNode tn_inst("triton_node", nh_inst, 115200, 32);
    tn_inst.Start();

    return EXIT_SUCCESS;
}