////////////////////////////////////////////////////////////////////////////////
/// @file TritonDriver.cpp
/// @author Arunabh Sharma <arunabh@locomation.ai>
/// @brief Class for the lucid vision driver
/// @version 0.1
/// @date 2019-03-21
///
/// @copyright
/// Copyright (c) 2019 Locomation, Inc.\n
/// All Rights Reserved.\n
/// CONFIDENTIAL and PROPRIETARY\n
/// @par
///
/// @internal
/// @par History
/// * 2019-03-21 Arunabh Sharma <arunabh@locomation.ai> Created file.
////////////////////////////////////////////////////////////////////////////////

#include "triton/TritonDriver.h"

#include <algorithm>
#include <cstring>
#include <memory>
#include <string>
#include <vector>

#include "Arena/ArenaApi.h"
// Disabling to avoid FFMPEG (for now)
//#include "Save/SaveApi.h"

#include "triton/TritonDefinitions.h"

namespace triton
{
  TritonDriver::TritonDriver()
  {
    InstantiateDefinitions();
  }

  bool TritonDriver::ConnectToDevice(const uint64_t mac_address,
                                     const int device_timeout)
  {
    bool device_connect_status = false;
    try
    {
      sys_inst_ = Arena::OpenSystem();
      sys_inst_->UpdateDevices(device_timeout);
      std::vector<Arena::DeviceInfo> device_info_vec = sys_inst_->GetDevices();
      std::cout << "No of cameras connected = " << device_info_vec.size()
                << std::endl;
      std::vector<Arena::DeviceInfo>::iterator device_search =
          std::find_if(device_info_vec.begin(),
                       device_info_vec.end(),
                       [&mac_address](Arena::DeviceInfo device_info) {
                         return device_info.MacAddress() == mac_address;
                       });
      if (device_search != device_info_vec.end())
      {
        cam_inst_             = sys_inst_->CreateDevice(*device_search);
        device_connect_status = true;
      }
      else
      {
        std::cout << "Correct mac address not specified"
                  << "\n";
      }
    }
    catch (GenICam::GenericException& ge)
    {
      std::cout << "GenICam exception thrown: " << ge.what() << "\n";
      device_connect_status = false;
    }
    catch (std::exception& ex)
    {
      std::cout << "Standard exception thrown: " << ex.what() << "\n";
      device_connect_status = false;
    }
    catch (...)
    {
      std::cout << "Unexpected exception thrown"
                << "\n";
      device_connect_status = false;
    }
    return device_connect_status;
  }  // namespace triton

  bool TritonDriver::ConfigureAndStartStream(
      const int img_width,
      const int img_height,
      const std::string& acquisition_mode,
      const std::string& buffer_handling_mode,
      const bool ptp_enable)
  {
    const std::string acquisition_mode_property = "AcquisitionMode";
    const std::string stream_buffer_handling_mode_property =
        "StreamBufferHandlingMode";
    const std::string ptp_enable_property       = "PtpEnable";
    const std::string ptp_slave_enable_property = "PtpSlaveOnly";

    // Set the acquisition mode as continuous so as to keep the stream from
    // stopping
    Arena::SetNodeValue<GenICam::gcstring>(cam_inst_->GetNodeMap(),
                                           acquisition_mode_property.c_str(),
                                           acquisition_mode.c_str());
    if (Arena::GetNodeValue<GenICam::gcstring>(
            cam_inst_->GetNodeMap(), acquisition_mode_property.c_str()) !=
        acquisition_mode.c_str())
    {
      std::cout << "Unable to set acquisition mode"
                << "\n";
      return false;
    }

    // Set the buffer mode to deliver only the latest frame even if it means
    // skipping frames
    Arena::SetNodeValue<GenICam::gcstring>(
        cam_inst_->GetTLStreamNodeMap(),
        stream_buffer_handling_mode_property.c_str(),
        buffer_handling_mode.c_str());
    if (Arena::GetNodeValue<GenICam::gcstring>(
            cam_inst_->GetTLStreamNodeMap(),
            stream_buffer_handling_mode_property.c_str()) !=
        buffer_handling_mode.c_str())
    {
      std::cout << "Unable to set buffer handling mode"
                << "\n";
      return false;
    }

    try
    {
      // Set Width of image
      GenApi::CIntegerPtr current_img_width =
          cam_inst_->GetNodeMap()->GetNode("Width");
      current_img_width->SetValue(img_width);
      // // Set Height of Image
      GenApi::CIntegerPtr current_img_height =
          cam_inst_->GetNodeMap()->GetNode("Height");
      current_img_height->SetValue(img_height);
    }
    catch (GenICam::GenericException& ge)
    {
      std::cout << "Unsupported image size"
                << "\n";
      std::cout << "GenICam exception thrown: " << ge.what() << "\n";
      return false;
    }
    // Enable the precision time protocol
    if (ptp_enable)
    {
      Arena::SetNodeValue<bool>(
          cam_inst_->GetNodeMap(), ptp_enable_property.c_str(), PTP_ENABLE);
      if (!Arena::GetNodeValue<bool>(cam_inst_->GetNodeMap(),
                                     ptp_enable_property.c_str()))
      {
        std::cout << "Unable to enable ptp"
                  << "\n";
      }
      Arena::SetNodeValue<bool>(cam_inst_->GetNodeMap(),
                                ptp_slave_enable_property.c_str(),
                                PTP_SLAVE_ENABLE);
      if (!Arena::GetNodeValue<bool>(cam_inst_->GetNodeMap(),
                                     ptp_slave_enable_property.c_str()))
      {
        std::cout << "Unable to set ptp device as slave"
                  << "\n";
      }
    }
    cam_inst_->StartStream();

    return true;
  }

  ImageStatus TritonDriver::GetImage(const int image_timeout,
                                     std::vector<uint8_t>& out_data)
  {
    img_buf_inst_ = cam_inst_->GetImage(image_timeout);
    std::cout << "Does image have data = " << img_buf_inst_->HasImageData()
              << std::endl;
    if (img_buf_inst_->HasImageData() && !img_buf_inst_->IsIncomplete() &&
        !img_buf_inst_->DataLargerThanBuffer())
    {
      buf_size_        = img_buf_inst_->GetSizeFilled();
      width_           = img_buf_inst_->GetWidth();
      height_          = img_buf_inst_->GetHeight();
      timestamp_       = img_buf_inst_->GetTimestampNs();
      bytes_per_pixel_ = img_buf_inst_->GetBitsPerPixel() / 8;

      std::cout << "image size = " << buf_size_ << "\n";
      std::cout << "height_ = " << height_ << "\n";
      std::cout << "width = " << width_ << "\n";

      std::cout << "Location of img_data_pointer = "
                << static_cast<void*>(&out_data[0]) << "\n";

      // std::copy(img_buf_inst_->GetData(),
      // img_buf_inst_->GetData() + width_ * height_,
      // std::back_inserter(out_data));
      std::memcpy(&out_data[0], img_buf_inst_->GetData(), buf_size_);
      std::cout << __FILE__ << " " << __LINE__ << " "
                << std::to_string(out_data[0]) << "\n";
      ReAllocateImageBuffer();
      if (out_data.size() > buf_size_)
      {
        std::cout << "Extra memory allocated for the output buffer"
                  << "\n";
        return BUFFER_UNDERFLOW_FRAME;
      }
    }
    else
    {
      if (!img_buf_inst_->HasImageData())
      {
        std::cout << "No image data found"
                  << "\n";
        return NO_IMAGE_DATA;
      }
      if (img_buf_inst_->IsIncomplete())
      {
        std::cout << "Frame is incomplete and image size = "
                  << img_buf_inst_->GetSizeFilled() << "\n";
        return INCOMPLETE_FRAME;
      }
      if (img_buf_inst_->DataLargerThanBuffer())
      {
        std::cout << "Frame is larger than and image size = "
                  << img_buf_inst_->GetSizeFilled() << "\n";
        return BUFFER_OVERFLOW_FRAME;
      }
    }
    return COMPLETE_FRAME;
  }

  void TritonDriver::ReAllocateImageBuffer()
  {
    cam_inst_->RequeueBuffer(img_buf_inst_);
  }

  void TritonDriver::DestroyDevice()
  {
    cam_inst_->StopStream();
    sys_inst_->DestroyDevice(cam_inst_);
    Arena::CloseSystem(sys_inst_);
  }

  /// DRA: Depens on FFMPEG, disabling for now
  void TritonDriver::SaveImage(const std::string ) //out_file_name)
  {
/*
    Arena::IImage* img_convert =
        Arena::ImageFactory::Convert(img_buf_inst_, BGR8);
    Save::ImageParams params(img_convert->GetWidth(),
                             img_convert->GetHeight(),
                             img_convert->GetBitsPerPixel());
    Save::ImageWriter writer(params, out_file_name.c_str());
    writer << img_convert->GetData();
    Arena::ImageFactory::Destroy(img_convert);
    */
  }

  uint64_t TritonDriver::GetTimestamp() const
  {
    return timestamp_;
  }

  size_t TritonDriver::GetWidth() const
  {
    return width_;
  }
  size_t TritonDriver::GetHeight() const
  {
    return height_;
  }

  size_t TritonDriver::GetBufferSize() const
  {
    return buf_size_;
  }

  void TritonDriver::SetExposureTime(const double exposure_time)
  {
    exposure_time_ = exposure_time;
    if (exposure_time_ - 0.0 > 1e-3)
    {
      std::cout << "Exposure can't be zero"
                << "\n";
      return;
    }
    Arena::SetNodeValue<GenICam::gcstring>(
        cam_inst_->GetNodeMap(),
        "ExposureAuto",
        ExposureStrategyVal[EXPOSURE_MANUAL].c_str());
    if (Arena::GetNodeValue<GenICam::gcstring>(cam_inst_->GetNodeMap(),
                                               "ExposureAuto") !=
        ExposureStrategyVal[EXPOSURE_MANUAL].c_str())
    {
      std::cout << "Unable to set the exposure to manual mode"
                << "\n";
    }

    Arena::SetNodeValue<double>(
        cam_inst_->GetNodeMap(), "ExposureTime", exposure_time_);
    if (Arena::GetNodeValue<double>(cam_inst_->GetNodeMap(), "ExposureTime") -
            exposure_time_ >
        1e-3)
    {
      std::cout << "Unable to set exposure time"
                << "\n";
    }
  }

  void TritonDriver::SetGain(const double gain)
  {
    gain_ = gain;
    Arena::SetNodeValue<GenICam::gcstring>(
        cam_inst_->GetNodeMap(),
        "GainAuto",
        GainStrategyVal[GAIN_MANUAL].c_str());
    if (Arena::GetNodeValue<GenICam::gcstring>(cam_inst_->GetNodeMap(),
                                               "GainAuto") !=
        GainStrategyVal[GAIN_MANUAL].c_str())
    {
      std::cout << "Unable to set the gain to manual mode"
                << "\n";
    }

    Arena::SetNodeValue<double>(cam_inst_->GetNodeMap(), "Gain", gain);
    if (Arena::GetNodeValue<double>(cam_inst_->GetNodeMap(), "Gain") - gain >
        1e-3)
    {
      std::cout << "Unable to set gain"
                << "\n";
    }
  }

  std::size_t TritonDriver::GetBytesPerPixel()
  {
    return bytes_per_pixel_;
  }

}  // namespace triton