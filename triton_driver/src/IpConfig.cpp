/***************************************************************************************
 *** ***
 ***  Copyright (c) 2018, Lucid Vision Labs, Inc. ***
 *** ***
 ***  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ****
 ***  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ****
 ***  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 *THE    ***
 ***  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER ***
 ***  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *FROM,  ***
 ***  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *THE  ***
 ***  SOFTWARE. ***
 *** ***
 ***************************************************************************************/

#include "Arena/ArenaApi.h"

#include <algorithm>
#include <iostream>

#define TIMEOUT 100

uint32_t ConvertIPv4ToInt(const std::string ip_address)
{
    const unsigned bits_per_term = 8;
    const unsigned num_terms     = 4;

    std::istringstream ip(ip_address);
    uint32_t packed = 0;

    for (unsigned i = 0; i < num_terms; ++i)
    {
        unsigned term;
        ip >> term;
        ip.ignore();

        packed += term << (bits_per_term * (num_terms - i - 1));
    }

    return packed;
}

void ForceNetworkSettings(Arena::ISystem* pSystem, const std::string in_ipv4)
{
    // discover devices
    std::cout << "Discover devices\n";

    std::cout << in_ipv4 << "\n";

    pSystem->UpdateDevices(TIMEOUT);
    std::vector<Arena::DeviceInfo> deviceInfos = pSystem->GetDevices();

    std::cout << "Get device information\n";

    const uint64_t macAddress = deviceInfos[0].MacAddress();
    // const uint32_t ipAddress      = deviceInfos[0].IpAddress();
    const uint32_t subnetMask     = deviceInfos[0].SubnetMask();
    const uint32_t defaultGateway = deviceInfos[0].DefaultGateway();

    std::cout << "MAC " << deviceInfos[0].MacAddressStr() << "\n";
    std::cout << "IP " << deviceInfos[0].IpAddressStr() << "\n";
    std::cout << "Subnet " << deviceInfos[0].SubnetMaskStr() << "\n";
    std::cout << "Gateway " << deviceInfos[0].DefaultGatewayStr() << "\n";

    uint32_t ipAddressToSet = ConvertIPv4ToInt(in_ipv4);
    std::cout << std::showbase << std::hex << ipAddressToSet << "\n";

    pSystem->ForceIp(macAddress, ipAddressToSet, subnetMask, defaultGateway);

    std::cout << "Discover devices again\n";

    pSystem->UpdateDevices(TIMEOUT);
    deviceInfos = pSystem->GetDevices();

    std::cout << "Get device information again\n";

    auto it = std::find_if(deviceInfos.begin(),
                           deviceInfos.end(),
                           [&macAddress](Arena::DeviceInfo deviceInfo) {
                               return deviceInfo.MacAddress() == macAddress;
                           });

    std::cout << "MAC " << (*it).MacAddressStr() << "\n";
    std::cout << "IP " << (*it).IpAddressStr() << "\n";
    std::cout << "Subnet " << (*it).SubnetMaskStr() << "\n";
    std::cout << "Gateway " << (*it).DefaultGatewayStr() << "\n";
}

int main(int argc, char* argv[])
{
    std::cout << "Cpp_ForceIp\n";

    if (argc < 2)
    {
        std::cout << "Usage : ./ip_config <ip-to-set-device-to>" << std::endl;
        return EXIT_FAILURE;
    }

    Arena::ISystem* pSystem = Arena::OpenSystem();
    pSystem->UpdateDevices(100);
    std::vector<Arena::DeviceInfo> deviceInfos = pSystem->GetDevices();
    if (deviceInfos.size() == 0)
    {
        std::cout << "\nNo camera connected\nPress enter to complete\n";
        std::getchar();
        return 0;
    }

    // run example
    std::cout << "Commence example\n\n";
    ForceNetworkSettings(pSystem, argv[1]);
    std::cout << "\nExample complete\n";

    // clean up example
    Arena::CloseSystem(pSystem);
    return EXIT_SUCCESS;
}
