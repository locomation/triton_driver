////////////////////////////////////////////////////////////////////////////////
/// @file DisplayVideo.cpp
/// @author Arunabh Sharma <arunabh@locomation.ai>
/// @brief Class for the displaying data from a lucid vision camera.
/// @version 0.1
/// @date 2019-03-21
///
/// @copyright
/// Copyright (c) 2019 Locomation, Inc.\n
/// All Rights Reserved.\n
/// CONFIDENTIAL and PROPRIETARY\n
/// @par
///
/// @internal
/// @par History
/// * 2019-03-21 Arunabh Sharma <arunabh@locomation.ai> Created file.
////////////////////////////////////////////////////////////////////////////////

#include "triton/TritonDriver.h"

#include <string>

int main()
{
  const int iw                                  = 2048;
  const int ih                                  = 1536;
  const std::string acquisition_mode            = "Continuous";
  const std::string stream_buffer_handling_mode = "NewestOnly";
  const bool ptp_enable                         = false;
  const double exposure_time                    = 30000.0;
  const uint64_t mac_address                    = 0x1c0faf437ef1;
  const int device_timeout                      = 100;
  const int image_timeout                       = 5000;

  triton::TritonDriver td_inst;
  td_inst.ConnectToDevice(mac_address, device_timeout);
  td_inst.ConfigureAndStartStream(
      iw, ih, acquisition_mode, stream_buffer_handling_mode, ptp_enable);
  td_inst.SetExposureTime(exposure_time);
  int count = -1;

  // uint8_t* data;
  // data = new uint8_t[2048 * 1536];

  std::vector<uint8_t> data;
  data.resize(iw * ih);

  while (count < 10 || count == -1)
  {
    td_inst.GetImage(image_timeout, data);
    // count++;
    // td_inst.ReAllocateImageBuffer();
  }
  td_inst.DestroyDevice();
  return 0;
}