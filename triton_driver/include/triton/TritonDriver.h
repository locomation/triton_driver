//------------------------------------------------------------------------------
/// @file TritonDriver.h
/// @author     Arunabh Sharma <arunabh@locomation.ai>
/// @brief      Class for the lucid vision driver
/// @version    0.1
/// @date       2019-03-21
///
/// @copyright  Copyright (c) 2019 Locomation, Inc.\n All Rights Reserved.\n
///             CONFIDENTIAL and PROPRIETARY\n
/// @par
///
/// @internal
/// @par History
/// * 2019-03-21 Arunabh Sharma <arunabh@locomation.ai> Created file.
///   //////////////////////////////////////////////////////////////////////////

#ifndef TRITONDRIVER_TRITON_TRITONDRIVER_H_
#define TRITONDRIVER_TRITON_TRITONDRIVER_H_

#include <cstdint>
#include <string>
#include <vector>

namespace Arena
{
  class IDevice;
  class IImage;
  class ISystem;
}  // namespace Arena

namespace triton
{
  enum ImageStatus : int;
  /**
   * @brief      Class for the lucid vision triton camera.
   *
   * @ingroup    camera
   * @author     Arunabh Sharma
   * @attention  This is for internal use only, DO NOT distribute the code
   */
  class TritonDriver
  {
   public:
    /**
     * @brief      Constructs the triton driver object
     */
    TritonDriver();

    /**
     * @brief      Destroys the object.
     */
    ~TritonDriver() = default;

    /**
     * @brief      Gets the camera device object from the arena sdk
     *
     * @param[in]  mac_address     The mac address of the camera
     * @param[in]  device_timeout  The device timeout for acquiring the camera
     *                             in milliseconds, the function will block for
     *                             that amount of time at worst
     *
     * @return     Whether the object for retrieved or not
     */
    bool ConnectToDevice(const uint64_t mac_address, const int device_timeout);
    /**
     * @brief      Configures the camera stream and starts it
     *
     * @param[in]  img_width             Desired width of image from camera
     * @param[in]  img_height            Desired height of image from camera
     * @param[in]  acquisition_mode      Acquisition mode to be configured on
     *                                   camera
     * @param[in]  buffer_handling_mode  Buffer handling mode to be configured
     *                                   on camera
     * @param[in]  ptp_enable            Enable PTP protocol on camera
     *
     * @return     Whether the configuration was successful or not
     */
    bool ConfigureAndStartStream(const int img_width,
                                 const int img_height,
                                 const std::string& acquisition_mode,
                                 const std::string& buffer_handling_mode,
                                 const bool ptp_enable);
    /**
     * @brief      Gets the image buffer from the camera object
     *
     * @param[in]  image_timeout  Timeout in acquiring an image from an active
     *                            camera, will block for the period at worst
     * @param      out_data       Image data from the buffer
     *
     * @return     Status of the image frame
     */
    ImageStatus GetImage(const int image_timeout,
                         std::vector<uint8_t>& out_data);

    /**
     * @brief      Saves an image from the arena sdk
     *
     * @param[in]  out_file_name  Name of file when saved
     */
    void SaveImage(const std::string out_file_name);

    /**
     * @brief      Destroys the system, device and buffer objects in reverse
     *             order using arena sdk
     */
    void DestroyDevice();

    /**
     * @brief      Gets the timestamp from the camera
     *
     * @return     The timestamp dictated by ptp protocol or otherwise in
     *             nanoseconds
     */
    uint64_t GetTimestamp() const;
    /**
     * @brief      Gets the width of the image from the camera
     *
     * @return     The width.
     */
    size_t GetWidth() const;
    /**
     * @brief      Gets the height of the image from the camera
     *
     * @return     The height.
     */
    size_t GetHeight() const;
    /**
     * @brief      Gets the size of the image/buffer in bytes
     *
     * @return     The buffer size.
     */
    size_t GetBufferSize() const;

    /**
     * @brief      Sets the exposure time of the camera and sets the auto
     *             exposure off in microseconds
     *
     * @param[in]  exposure_time  The exposure time
     */
    void SetExposureTime(const double exposure_time);

    /**
     * @brief      Sets the gain of the camera and switches auto gain off
     *
     * @param[in]  gain  The gain
     */
    void SetGain(const double gain);

    /**
     * @brief      Gets the bytes per pixel.
     *
     * @return     The bytes per pixel.
     */
    std::size_t GetBytesPerPixel();

    /**
     * @brief      Reallocates the image buffer and preps the camera sdk for a
     *             new image
     */
    void ReAllocateImageBuffer();

   private:
    Arena::IDevice* cam_inst_;     ///< Device/Camera object from arena sdk
    Arena::IImage* img_buf_inst_;  ///< Image buffer from arena sdk
    Arena::ISystem* sys_inst_;     ///< System environment object from arena sdk
    int device_timeout_;  ///< Timeout in milliseconds of retrieving device
                          ///< information
    int image_timeout_;   ///< Timeout in milliseconds of retrieving image
                          ///< information from device
    uint64_t timestamp_;  ///< Timestamp of image/buffer
    uint64_t mac_address_;
    std::size_t width_;            ///< Width of iamge
    std::size_t height_;           ///< Height of image
    std::size_t buf_size_;         ///< Size of buffer in image
    std::size_t bytes_per_pixel_;  ///< Number of bytes per pixel of the camera
    double exposure_time_;         ///< exposure time of camera
    double gain_;                  ///< gain of camera

  };  // class TritonDriver
}  // namespace triton

#endif  // TRITONDRIVER_TRITON_TRITONDRIVER_H_