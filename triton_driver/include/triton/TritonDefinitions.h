//------------------------------------------------------------------------------
/// @file TritonDefinitions.h
/// @author     Arunabh Sharma <arunabh@locomation.ai>
/// @brief      Header containing definitions for the configuration parameters
///             of the camera
/// @version    0.1
/// @date       2019-04-12
///
/// @copyright  Copyright (c) 2019 Locomation, Inc.\n All Rights Reserved.\n
///             CONFIDENTIAL and PROPRIETARY\n
/// @par
///
/// @internal
/// @par History
/// * 2019-04-12 Arunabh Sharma <arunabh@locomation.ai> Created file.
///   //////////////////////////////////////////////////////////////////////////

#ifndef TRITONDRIVER_TRITON_TRITONDEFINITIONS_H_
#define TRITONDRIVER_TRITON_TRITONDEFINITIONS_H_

#include <iostream>
#include <string>
#include <unordered_map>

namespace triton
{
  template <typename K, typename V>
  void PrintMap(std::unordered_map<K, V> const& m)
  {
    for (auto it = m.cbegin(); it != m.cend(); ++it)
    {
      std::cout << "{" << (*it).first << ": " << (*it).second << "}\n";
    }
  }

  /**
   * @brief      Gives the status of the image read from the camera
   */
  enum ImageStatus : int
  {
    COMPLETE_FRAME   = 0,  ///< Normal complete frame
    NO_IMAGE_DATA    = 1,  ///< No image data found
    INCOMPLETE_FRAME = 2,  ///< Incomplete frame read from camera
    BUFFER_OVERFLOW_FRAME =
        3,  ///< Camera frame size greater than destination buffer size
    BUFFER_UNDERFLOW_FRAME =
        4  ///< Destination buffer size greater than camera frame size
  };
  // Map of descriptions
  std::unordered_map<int, std::string> ImageStatusDesc;

  enum AcquisitionMode
  {
    ACQUISITION_CONTINUOUS =
        0  ///< Setup the camera for continuously getting images
  };
  // Map of values
  std::unordered_map<int, std::string> AcquisitionModeVal;
  // Map of descriptions
  std::unordered_map<int, std::string> AcquisitionModeDesc;

  enum StreamBufferHandlingMode
  {
    NEWEST_ONLY           = 0,
    OLDEST_FIRST          = 1,
    OLDEST_FIRSTOVERWRITE = 2
  };
  // Map of values
  std::unordered_map<int, std::string> StreamBufferHandlingModeVal;
  // Map of descriptions
  std::unordered_map<int, std::string> StreamBufferHandlingModeDesc;

  enum PTPEnable : bool
  {
    PTP_ENABLE  = true,
    PTP_DISABLE = false
  };
  // Map of descriptions
  std::unordered_map<bool, std::string> PTPEnableDesc;

  enum PTPSlaveMode : bool
  {
    PTP_SLAVE_ENABLE  = true,
    PTP_SLAVE_DISABLE = false
  };
  // Map of descriptions
  std::unordered_map<bool, std::string> PTPSlaveModeDesc;

  enum ExposureStrategy
  {
    EXPOSURE_MANUAL     = 0,
    EXPOSURE_CONTINUOUS = 1
  };
  // Map of values
  std::unordered_map<int, std::string> ExposureStrategyVal;
  // Map of descriptions
  std::unordered_map<int, std::string> ExposureStrategyDesc;

  enum GainStrategy
  {
    GAIN_MANUAL     = 0,
    GAIN_CONTINUOUS = 1
  };
  // Map of values
  std::unordered_map<int, std::string> GainStrategyVal;
  // Map of descriptions
  std::unordered_map<int, std::string> GainStrategyDesc;

  void GetDescription(const std::string& parameter_name)
  {
    std::cout << "All possible values of " << parameter_name << "\n";
    if (parameter_name == "AcquisitionMode")
    {
      PrintMap(AcquisitionModeDesc);
    }
    else if (parameter_name == "StreamBufferHandlingMode")
    {
      PrintMap(StreamBufferHandlingModeDesc);
    }
    else if (parameter_name == "PTPEnable")
    {
      PrintMap(PTPEnableDesc);
    }
    else if (parameter_name == "PTPSlaveMode")
    {
      PrintMap(PTPSlaveModeDesc);
    }
    else if (parameter_name == "ExposureAuto")
    {
      PrintMap(ExposureStrategyDesc);
    }
    else if (parameter_name == "GainAuto")
    {
      PrintMap(GainStrategyDesc);
    }
  }

  void InstantiateDefinitions()
  {
    ImageStatusDesc[ImageStatus::COMPLETE_FRAME] =
        "COMPLETE_FRAME : Normal Image frame";
    ImageStatusDesc[ImageStatus::NO_IMAGE_DATA] =
        "NO_IMAGE_DATA : No data in acquired image";
    ImageStatusDesc[ImageStatus::INCOMPLETE_FRAME] =
        "INCOMPLETE_FRAME : The frame has missing bytes";
    ImageStatusDesc[ImageStatus::BUFFER_OVERFLOW_FRAME] =
        "BUFFER_OVERFLOW_FRAME : Image has more bytes than the destination "
        "buffer";
    ImageStatusDesc[ImageStatus::BUFFER_UNDERFLOW_FRAME] =
        "BUFFER_UNDERFLOW_FRAME : Destination buffer has more bytes than the "
        "image requires";

    AcquisitionModeVal[AcquisitionMode::ACQUISITION_CONTINUOUS] = "Continuous";
    AcquisitionModeDesc[AcquisitionMode::ACQUISITION_CONTINUOUS] =
        "ACQUISITION_CONTINUOUS : Continuous mode for ongoing image "
        "acquisition";

    StreamBufferHandlingModeVal[StreamBufferHandlingMode::NEWEST_ONLY] =
        "NewestOnly";
    StreamBufferHandlingModeVal[StreamBufferHandlingMode::OLDEST_FIRST] =
        "OldestFirst";
    StreamBufferHandlingModeVal
        [StreamBufferHandlingMode::OLDEST_FIRSTOVERWRITE] =
            "OldestFirstOverwrite";
    StreamBufferHandlingModeDesc[StreamBufferHandlingMode::NEWEST_ONLY] =
        "NEWEST_ONLY : Get the latest frame from the queue regardless of "
        "skipping frames";
    StreamBufferHandlingModeDesc[StreamBufferHandlingMode::OLDEST_FIRST] =
        "OLDEST_FIRST : Not sure what this means yet";
    StreamBufferHandlingModeDesc
        [StreamBufferHandlingMode::OLDEST_FIRSTOVERWRITE] =
            "OLDEST_FIRSTOVERWRITE : Not sure what this means yet";

    PTPEnableDesc[PTPEnable::PTP_ENABLE] =
        "PTP_ENABLE : Enables the ptp protocol on the camera";
    PTPEnableDesc[PTPEnable::PTP_DISABLE] =
        "PTP_DISABLE : Disables the ptp protocol on the camera";

    PTPSlaveModeDesc[PTPSlaveMode::PTP_SLAVE_ENABLE] =
        "PTP_SLAVE_ENABLE : Makes the camera a slave to the ptp clock";
    PTPSlaveModeDesc[PTPSlaveMode::PTP_SLAVE_DISABLE] =
        "PTP_SLAVE_DISABLE : Makes the camera it's own master ";

    ExposureStrategyVal[ExposureStrategy::EXPOSURE_MANUAL]     = "Off";
    ExposureStrategyVal[ExposureStrategy::EXPOSURE_CONTINUOUS] = "Continuous";
    ExposureStrategyDesc[ExposureStrategy::EXPOSURE_MANUAL] =
        "MANUAL : Switch the camera to a user specified exposure time";
    ExposureStrategyDesc[ExposureStrategy::EXPOSURE_CONTINUOUS] =
        "CONTINUOUS : Camera decides the exposure time and adjusts it "
        "dynamically";

    GainStrategyVal[GainStrategy::GAIN_MANUAL]     = "Off";
    GainStrategyVal[GainStrategy::GAIN_CONTINUOUS] = "Continuous";
    GainStrategyDesc[GainStrategy::GAIN_MANUAL] =
        "MANUAL : Switch the camera to a user specified gain value";
    GainStrategyDesc[GainStrategy::GAIN_CONTINUOUS] =
        "CONTINUOUS : Camera decides the gain and adjusts it dynamically";
  }

}  // namespace triton

#endif  // TRITONDRIVER_TRITON_TRITONDEFINITIONS_H_